package com.teamc.mocktwo;

import com.teamc.mocktwo.model.User;
import com.teamc.mocktwo.repository.ProductRepository;
import com.teamc.mocktwo.repository.RoleRepository;
import com.teamc.mocktwo.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class ShoppingCartApplication {

	public static void main(String[] args) {
		
		ApplicationContext context = SpringApplication.run(ShoppingCartApplication.class, args);
		
		UserRepository userRepository = context.getBean(UserRepository.class);
		
		RoleRepository roleRepository = context.getBean(RoleRepository.class);
		
		ProductRepository productRepository = context.getBean(ProductRepository.class);
		
		PasswordEncoder encoder = context.getBean(PasswordEncoder.class);
		
		User user1 = new User(Long.valueOf(1), "admin", encoder.encode("admin"),"Ngô Sơn Tùng", "sontung29695@gmail.com", "0987654321", "secret", true);
		
		userRepository.save(user1);
	}
	
}
