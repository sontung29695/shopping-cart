package com.teamc.mocktwo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "username", nullable = false, length = 45)
    private String username;
    
    @Column(name = "password", nullable = false, length = 120)
    private String password;
    
    @Column(name = "fullname", nullable = false, length = 45)
    private String fullname;
    
    @Column(name = "email", nullable = false, length = 45)
    private String email;
    
    @Column(name = "phone", nullable = false, length = 45)
    private String phone;
    
    @Column(name = "secret", nullable = false, length = 45)
    private String secret;
    
    @Column(name = "status", nullable = false)
    private Boolean status;
    
}