package com.teamc.mocktwo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "product")
public class Product {
    @Id
    @Column(name = "product_id", nullable = false)
    private Long id;
    
    @Column(name = "product_name", nullable = false, length = 45)
    private String product_name;
    
    @Column(name = "product_price", nullable = false)
    private Long product_price;
    
    @Column(name = "product_quantity", nullable = false)
    private Long product_quantity;
}