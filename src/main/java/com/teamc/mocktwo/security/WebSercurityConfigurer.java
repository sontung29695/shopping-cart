package com.teamc.mocktwo.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity //su dung de kich hoat spring security
//@EnableGlobalMethodSecurity(
//        // securedEnabled = true,
//        // jsr250Enabled = true,
//        prePostEnabled = true)
public class WebSercurityConfigurer extends WebSecurityConfigurerAdapter {
//    @Autowired
//    UserDetailsServiceImpl userDetailsService;
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
//    @Bean
//    public CustomAuthorizationFilter authenticationJwtTokenFilter() {
//        return new CustomAuthorizationFilter();
//    }
//    @Autowired
//    private AuthEntryPointJwt unauthorizedHandler;
    
//    @Bean
//    @Override
//    public AuthenticationManager authenticationManagerBean() throws Exception {
//        return super.authenticationManagerBean();
//    }
//
//    @Override // AuthenticationManagerBuilder dùng để cấu hình các AuthenticationManager được tạo ra
//              // (ví dụ như nó lấy User từ đâu, tạo ra đối tượng UserDetails ntn và cấu hình password như thế nào
//    public void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService)
//                .passwordEncoder(passwordEncoder());
//    }
//
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
//        customAuthenticationFilter.setFilterProcessesUrl("/api/loginCustom");
//        http.authorizeRequests()
//                .antMatchers("/").permitAll()
//                .antMatchers("/api/user").hasRole("USER")
//                .antMatchers("/api/admin").hasRole("ADMIN")
//                .antMatchers("/api/user/*").hasRole("USER")
//                .antMatchers("/api/admin/*").hasRole("ADMIN")
//                .anyRequest().authenticated()
//                .and()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
//                .and()
//                .csrf().disable() // chú ý khi debug bằng postman phải thêm header cho csrf
//                .formLogin()
//                .loginPage("/api/login")
//                .defaultSuccessUrl("/")
//                .permitAll();
//            http.addFilterAt(customAuthenticationFilter,UsernamePasswordAuthenticationFilter.class);
//            http.addFilterBefore(authenticationJwtTokenFilter(), CustomAuthenticationFilter.class);
        
        
//        http.cors().and().csrf().disable()
//                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//                .authorizeRequests().antMatchers("/api/auth/**").permitAll()
//                .antMatchers("/api/test/**").permitAll()
//                .anyRequest().authenticated();
//        // UsernamePasswordAuthenticationFilter
//        http.addFilterAt(new CustomAuthenticationFilter(authenticationManagerBean()),UsernamePasswordAuthenticationFilter.class);
//        //http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
//    }
}
