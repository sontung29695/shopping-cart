package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
}