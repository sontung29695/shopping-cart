package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.model.Cart;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<Cart, Long> {
}