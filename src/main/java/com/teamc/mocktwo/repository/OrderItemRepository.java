package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.model.OrderItem;
import org.springframework.data.repository.CrudRepository;

public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {
}