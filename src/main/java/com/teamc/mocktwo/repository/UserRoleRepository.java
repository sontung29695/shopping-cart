package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.model.UserRole;
import org.springframework.data.repository.CrudRepository;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
}