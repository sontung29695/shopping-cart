package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.model.ProductReview;
import org.springframework.data.repository.CrudRepository;

public interface ProductReviewRepository extends CrudRepository<ProductReview, Long> {
}