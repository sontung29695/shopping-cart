package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}