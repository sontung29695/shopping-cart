package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}