package com.teamc.mocktwo.repository;

import com.teamc.mocktwo.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}